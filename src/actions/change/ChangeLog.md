# 2015-05-04  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added handling mechanism for UDPDataIndication:ControlInfo object
- added handling mechanism for UDPErrorIndication:ControlInfo object


# 2015-05-03  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Fixed
- fixed an issue with the the global filter, now the sending field is changed into the local filter


# 2015-04-26  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added handling mechanism for UDPSendCommand:ControlInfo object
- added handling mechanism for UDPBindCommand:ControlInfo object
- added handling mechanism for UDPConnectCommand:ControlInfo object
- added handling mechanism for UDPCloseCommand:ControlInfo object


# 2015-04-22  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added base handling mechanism to change control info and sending info


# 2015-04-14  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Fixed
- fixed issues in involvedLayer setting


# 2015-04-10  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Changed
- changed minor details


# 2015-04-09  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added Change class

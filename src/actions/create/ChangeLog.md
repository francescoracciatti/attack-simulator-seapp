# 2015-05-04  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added handling mechanism for UDPDataIndication:ControlInfo object
- added handling mechanism for UDPErrorIndication:ControlInfo object


# 2015-04-27  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added handling mechanism for UDPSendCommand:ControlInfo object
- added handling mechanism for UDPBindCommand:ControlInfo object
- added handling mechanism for UDPConnectCommand:ControlInfo object
- added handling mechanism for UDPCloseCommand:ControlInfo object


# 2015-04-22  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added base handling mechanism for ControlInfo objects


# 2015-04-14  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added Create class

# 2015-04-22  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added complete handling mechanism for UDP to change control info and sending info in change


# 2015-04-22  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added base handling mechanism to change control info and sending info in change


# 2015-04-15  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added retrieve
- added asfexpression


# 2015-04-14  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added create
- added put
### Fixed
- fixed issues in clone
- fixed issues in change
### Changed
- changed send handling in localfilter


# 2015-04-13  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Changed
- changed minor detail in send


# 2015-04-12  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added send


# 2015-04-10 Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Changed
- changed minor details in change


# 2015-04-09  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added clone
- added change
### Changed
- changed minor detail in drop


# 2015-03-16  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added involvedLayer attribute in actionbase
- added getInvolvedLayer method in actionbase


# 2015-03-11  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added drop action


# 2015-03-04  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added utility function toString to convert ActionType to string
- added utility function toActionType to convert string to ActionType


# 2015-03-01  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Fixed
- fixed a nullptr check_and_cast error in Move::execute
### Changed
- changed ActionName type in ActionType
- changed actionName attribute of ActionBase in actionType


# 2015-02-28  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Fixed
- fixed destroy constructor and execute method for code consistency
- fixed move action in case of no mobility sub-module in nodes


# 2015-02-26  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Changed
- changed actionbase, partial implementation
- changed destroy action, implemented
- changed move action, implemented


# 2015-02-25  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added actionbase
- added destroy action
- added move action

# 2015-05-13  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Changed
- minor changes


# 2015-04-15  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added SEAPPExpression class

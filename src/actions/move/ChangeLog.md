# 2015-03-07  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Fixed
- fixed  minor details in Move.cc


# 2015-03-01  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Fixed
- fixed minor issues in info display


# 2015-03-01  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Fixed
- fixed a nullptr check_and_cast error in Move::execute
### Changed
- changed ActionName param in action_t


# 2015-02-28  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Fixed
- fixed execute method in case of no mobility sub-module in nodes
### Changed
- changed Doxygen docs


# 2015-02-26  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
## Added
- added targetPosition attribute
- added targetNode attribute
### Changed
- changed execute method
- changed Doxygen docs


# 2015-02-25  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added Move.h with constructor, destructor and execute method
- added Move.cc, Move is a dummy class
- added Doxygen docs

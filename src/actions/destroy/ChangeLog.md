# 2015-03-07  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Changed
- changed minor details


# 2015-03-07  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Fixed
- fixed  minor details in Destroy.cc


# 2015-03-06  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Removed
- removed isDestroyed reference
### Added
- added targetNode reference 
### Changed
- changed constructor and execute method


# 2015-03-04  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Changed
- changed ActionName type in ActionType 

# 2015-02-28  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added isDestroyed attribute
### Fixed
- fixed destroy constructor for code consistency, now it initializes isDestroy attribute
- fixed execute method for code consistency, now it works with isDestroy attribute

# 2015-02-26  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Changed
- changed execute method


# 2015-02-25  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added Destroy.h with constructor, destructor and execute method
- added Destroy.cc, Destroy is a dummy class

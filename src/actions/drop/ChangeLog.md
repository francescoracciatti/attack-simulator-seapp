# 2015-04-09  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Changed
- changed minor details


# 2015-03-11  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added Drop.h
- added Drop.cc
- added constructor, destructor, execute method

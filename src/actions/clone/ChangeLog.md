# 2015-04-14  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### fixed
- fixed issues in involvedLayer setting

# 2015-04-09  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added Clone class

# 2015-04-09 Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added NONE_LAYER
### Changed
- changed minor details


# 2015-03-30 Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added setPacketName method
- added getPacketName method


# 2015-03-16 Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added involvedLayer attribute
- added getInvolvedLayer method
### Changed
- changed minor details


# 2015-03-11  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Changed
- changed minor details


# 2015-03-01  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Changed
- changed ActionName type in action_t
- changed actionName attribute in actionType


# 2015-02-28  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Fixed
- fixed the access modifier of the constructor, now it is protected
- fixed the width of the type ActionName for code portability


# 2015-02-26  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Changed
- changed constructor, it initialize actionName
- changed Doxygend doc


# 2015-02-25  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added ActionBase.h with constructor and destructor
- added ActionBase.cc, ActionBase is a dummy class
- added ActionName type
- added actionName attribute
- added DESTROY and MOVE to ActionName
- added Doxygen doc

# 2015-04-15  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added put handler in conditionalattacks
- added retrieve handler in conditionalattacks
- added expression handler in conditionalattacks
- added unconditionalattak


# 2015-04-14  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Fixed
- fixed issues in clone handler in conditionalattacks
- fixed issues in change handler in conditionalattacks


# 2015-04-13  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Fixed
- fixed issues in conditionalattack


# 2015-04-12  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added send handler in conditionalattacks


# 2015-04-09  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added clone handler in conditionalattack
- added change handler in conditionalattack
### Changed
- changed minor detail in drop handler in conditionalattacks


# 2015-04-02  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added drop handler in conditionalattack


# 2015-03-18  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Changed
- changed methods implementation in conditionalattack


# 2015-03-16  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added attributes methods in conditionalattack


# 2015-03-15  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added conditionalattack


# 2015-03-07  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added variableTable attribute to attackbase
- added addVariableTable method to attackbase


# 2015-03-07  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Changed
- changed AttackType to attack_t in AttackBase.h
### Fixed
- fixed minor details in physicalattack
- fixed minor details in attackbase


# 2015-03-01  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added utility functions in attackbase


# 2015-03-01  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added attackbase
- added physical
- added attackentry

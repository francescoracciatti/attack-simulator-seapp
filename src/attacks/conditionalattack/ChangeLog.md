# 2015-04-14  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added put handler
- added retrieve handler
- added expression handler


# 2015-04-14  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Fixed
- fixed issues in clone handler
- fixed issues in change handler


# 2015-04-13  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Fixed
- fixed minor issues


# 2015-04-12  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added send handler


# 2015-04-09  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added clone handler
- added change handler
### Changed
- changed minor detail in drop handler


# 2015-04-02  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added drop handler


# 2015-03-18  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Changed
- changed isMatchingFilter methods to improve its performances


# 2015-03-15  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added packetFilter and minimumLayerInvolved attribute
- added addAction, getFilter, setFilter and isMatchingFilter methods


# 2015-03-15  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added ConditionalAttack.h
- added ContidionalAttack.cc
- added constructor
- added destructor
- added execute method

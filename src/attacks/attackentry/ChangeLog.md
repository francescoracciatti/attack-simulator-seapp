# 2015-03-01  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- Added AttackEntry.h
- Added AttackEntry.cc
- Added occurrenceTime attribute, attack attribute
- Added constructor, destructor, getOccurrenceTime method, getAttack method

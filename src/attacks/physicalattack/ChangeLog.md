# 2015-03-07  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Fixed
- fixed minor details in PhysicalAttack.cc


# 2015-03-01  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added PhysicalAttack.h
- added PhysicalAttack.cc
- added constructor
- added destructor
- added execute method, it executes all the actions that make the attack

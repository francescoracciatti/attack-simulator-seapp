# 2015-04-27  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Fixed
- fixed minor issues


# 2015-03-07  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added variableTable attribute
- added addVariableTable method


# 2015-03-07  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Changed
- changed AttackType to attack_t


# 2015-03-01  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added utility function toString to convert AttackType to string
- added utility function toAttackType to convert string to AttackType


# 2015-03-01  Francesco Racciatti  <racciatti.francesco@gmail.com>
## [Unreleased]
### Added
- added AttackBase.h
- added AttackBase.cc
- added constructor
- added destructor
- added AttackType type
- added attackType attribute, i.e. the type of the attack
- added baseActions attribute, i.e. the list of the actions that make the attack

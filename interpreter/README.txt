Authors:
Alessandro Pischedda	alessandro.pischedda@gmail.com
Marco Tiloca		marco.tiloca84@gmail.com

Usage
----------

	$ python interpreter.py -o <file_output> <file_input>

Produce the XML attack configuration file for Castalia, according to the attack description in the input file.


Example
-------

	$ python interpreter.py -o output.xml description


 
